import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApiComponent } from './core/comopnents/routed/api/api.component';
import { ResourcesComponent } from './core/comopnents/routed/resources/resources.component';
import { LoginComponent } from './core/comopnents/routed/login/login.component';
import { AuthGuardService } from './core/services/auth-guard.service';
import { ResourceComponent } from './core/comopnents/routed/resource/resource.component';
import { ItemComponent } from './core/comopnents/routed/item/item.component';
import { SearchComponent } from './core/comopnents/routed/search/search.component';
import { CacheComponent } from './core/comopnents/routed/cache/cache.component';

const routes: Routes = [
	{
		path: 'login',
		component: LoginComponent,
	},
	{
		path: '',
		component: ApiComponent,
		canActivate: [AuthGuardService]
	},
	{
		path: 'api/:api',
		component: ApiComponent,
		canActivate: [AuthGuardService],
		children: [
			{
				path: 'resources',
				component: ResourcesComponent
			},
			{
				path: 'resource/:resource',
				children: [
					{
						path: '',
						redirectTo: '1',
						pathMatch: 'full'
					},
					{
						path: ':page',
						component: ResourceComponent
					}
				]
			},
			{
				path: 'item/:resource/:id',
				component: ItemComponent
			},
			{
				path: 'search/:term',
				component: SearchComponent,
			}
		]
	},
	{
		path: 'cache',
		component: CacheComponent,
		canActivate: [AuthGuardService]
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'top' })],
	exports: [RouterModule]
})
export class AppRoutingModule { }
