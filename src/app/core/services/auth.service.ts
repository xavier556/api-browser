import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	// logged in flag
	private isLoggedIn: boolean;

	// user name
	public username: string;

	/**
	 * Constructor.
	 * Retrieve the username from local storage if present.
	 * @param router 
	 */
	constructor(public router: Router) {
		const username = localStorage.getItem('appapi-username');
		if (username) {
			this.isLoggedIn = true;
			this.username = username;
		}
		else {
			this.isLoggedIn = false;
			this.username = 'Anonymous';
		}
	}

	/**
	 * Tells if the user is logged in or not.
	 * @returns {boolean}
	 */
	public isAuthenticated(): boolean {
		return this.isLoggedIn;
	}

	/**
	 * Save the username to local storge and set the login flag to true.
	 * The user is redirected to the /home route.
	 * @param username 
	 */
	public login(username: string) {
		localStorage.setItem('appapi-username', username);
		this.username = username;
		this.isLoggedIn = true;
		this.router.navigate(['']);
	}

	/**
	 * Remove the username from local storage and set the login flag to
	 * false. The user is redirected to the /login route.
	 */
	public logout() {
		localStorage.removeItem('star-wars-fan-app-user');
		this.username = 'Anonymous';
		this.isLoggedIn = false;
		this.router.navigate(['login']);
		return false;
	}
}
