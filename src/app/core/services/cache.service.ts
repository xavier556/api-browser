import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class CacheService {

	private prefix: string = 'appcache';

	constructor(private http: HttpClient) { }

	getCachedUrl(api: string, url: string): string {
		const key: string = this.prefix + '-' + api + '-' + url;
		const cached: string = localStorage.getItem(key);
		return cached;
	}

	cacheUrl(api: string, url: string, data: string) {
		const key: string = this.prefix + '-' + api + '-' + url;
		localStorage.setItem(key, data);
	}

	getApiCache(api: string) {

		const urls = [];

		// iterate on each localstorage entires
		for (let i = 0; i < localStorage.length; i++) {

			// keys have the following format:
			// AppPrefix-ApiPrefix-Url
			const key = localStorage.key(i);
			const parts = key.split('-');
			const appPrefix = parts[0];
			const apiPrefix = parts[1];
			const url = parts[2];
			if (appPrefix && apiPrefix && url) {

				// everything cached by the app starts with the prefix
				if (appPrefix === this.prefix && apiPrefix === api) {

					// add entry
					urls.push(url);

				}
			}
		}

		return urls;
	}

	clearApiCache(api: string): number {

		// First gather all the keys to be deleted. They start
		// with AppPrefix-ApiPrefix

		const prefix = this.prefix + '-' + api

		let arr = [];

		let counter: number = 0;

		for (let i = 0; i < localStorage.length; i++) {
			const key = localStorage.key(i);
			if (key.startsWith(prefix)) {
				arr.push(key);
			}
		}

		// Delete those entries from the local cache.
		arr.forEach(key => {
			localStorage.removeItem(key);
			counter++
		});

		return counter;
	}

	loadApiCache(api: string): Observable<number> {

		const fileName: string = 'assets/cache/' + api + '.cache.json';

		return this.http.get(fileName).pipe(map(json => {
			let counter: number = 0;
			for (const url in json) {
				const key = this.prefix + '-' + api + '-' + url;
				if (!localStorage.getItem(key)) {
					localStorage.setItem(key, json[url]);
					counter++;
				}
			}
			return counter;
		}));
	}

	/**
	 * Create and download a json file of the cached entries.
	 */
	public downloadApiCache(api: string) {
		const element = document.createElement('a');
		element.setAttribute('href', 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(this.getApiCache(api))));
		element.setAttribute('download', 'cache.json');
		element.style.display = 'none';
		document.body.appendChild(element);
		element.click();
		document.body.removeChild(element);
	}
}
