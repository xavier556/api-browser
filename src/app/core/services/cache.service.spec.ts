import { TestBed } from '@angular/core/testing';

import { CacheService } from './cache.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CacheService', () => {
	beforeEach(() => TestBed.configureTestingModule({
		imports: [
			HttpClientTestingModule,

		],
	}));

	it('should be created', () => {
		const service: CacheService = TestBed.get(CacheService);
		expect(service).toBeTruthy();
	});
});
