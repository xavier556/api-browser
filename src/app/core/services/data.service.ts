import { Injectable, Injector } from '@angular/core';
import { IApiService, IItem, IResource } from '../api/api.interface';
import { HttpClient } from '@angular/common/http';
import { apiNameToken, serviceToken, serviceFactory } from '../api/api.factory';
import { ApiRegistery } from '../api/api.registery';
import { CacheService } from './cache.service';
import { Observable, of, Subject, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

const apiNotSetError: Error = new Error('Api is not set.');

@Injectable({
	providedIn: 'root'
})
export class DataService {

	/**
	 * ============================================================================================
	 * PROPERTIES
	 * ============================================================================================
	 */

	// The current api service.
	private api: IApiService = null;

	// For other component to subscribe to api change.
	private apiChangeSubject: Subject<string>;
	private apiChange: Observable<string>;

	// The list of available api's.
	public listOfApi: { [key: string]: string } = {};

	/**
	 * ============================================================================================
	 * CONSTRUCTOR
	 * ============================================================================================
	 */

	/**
	 * Constructor.
	 * @param {HttpClient} http Angular http service.
	 * @param {CacheService} cache Cache service.
	 */
	constructor(private http: HttpClient, private cache: CacheService) {

		// Create a public list of the available api's from the api registery.
		// The keys are the technical names, the values the friendly displayable names.
		for (let key of Object.keys(ApiRegistery)) {
			this.listOfApi[ApiRegistery[key].meta.route] = ApiRegistery[key].meta.name;
		}

		this.apiChangeSubject = new Subject<string>();
		this.apiChange = this.apiChangeSubject.asObservable();
	}

	/**
	 * ============================================================================================
	 * PUBLIC
	 * ============================================================================================
	 */

	getApiRoute(): string {
		return this.api.meta.route;
	}

	getApiMeta(name: string): string {

		// Make sure there's an api.
		if (!this.api) return '';

		return this.api.meta[name] || null;
	}

	/**
	 * Return the list of resources for the current api.
	 * @returns {Observable<string[]>}
	 */
	getResources(): Observable<IResource[]> {

		// Make sure there's an api.
		if (!this.api) return throwError(apiNotSetError);

		// Return resources from api.
		return this.api.getResources()

			// Catch any error
			.pipe(catchError(error => throwError(error)));

	}

	/**
	 * Return one page of a resource of the current api.
	 * @param {string} resource Resource name.
	 * @param {number} page Page number, default is 1.
	 * @returns {Observable<IResource>}
	 */
	getResource(resource: string, page: number = 1): Observable<IResource> {

		// Make sure there's an api.
		if (!this.api) return throwError(apiNotSetError);

		// Return resource from api.
		return this.api.getResource(resource, page)

			// Catch any error
			.pipe(catchError(error => throwError(error)));
	}

	/**
	 * Return one item of a resource of the current api.
	 * @param {string} resource Resource name.
	 * @param {number} id Item id.
	 * @returns {Observable<IItem>}
	 */
	getItem(resource: string, id: number): Observable<IItem> {

		// Make sure there's an api.
		if (!this.api) return throwError(apiNotSetError);

		// Return item from api.
		return this.api.getItem(resource, id)

			// Catch any error
			.pipe(catchError(error => throwError(error)));
	}

	/**
	 * Return resources with items matching the search term.
	 * @param {string} term The search term.
	 * @returns {Observable<IResource[]}
	 */
	search(term: string): Observable<IResource[]> {

		// Make sure there's an api.
		if (!this.api) return throwError(apiNotSetError);

		// Return results from the api.
		return this.api.search(term)

			// Catch any error
			.pipe(catchError(error => throwError(error)));
	}

	/**
	 * Set the current api service.
	 * The api must be present in the ApiRegistery.
	 * The service is created dynamically using an injector.
	 * @param name 
	 */
	setApi(name: string) {

		const inj = Injector.create({
			providers: [
				{ provide: CacheService, useValue: this.cache, deps: [] },
				{ provide: HttpClient, useValue: this.http, deps: [] },
				{ provide: apiNameToken, useValue: name, deps: [] },
				{ provide: serviceToken, useFactory: serviceFactory, deps: [apiNameToken, HttpClient, CacheService] },
			]
		});

		this.api = inj.get(serviceToken);

		this.apiChangeSubject.next(name);
	}

	/**
	 * Clear the current api.
	 */
	removeApi() {
		this.api = null;
	}

	/**
	 * 
	 */
	getApiChangeObservable(): Observable<string> {
		return this.apiChange;
	}

	/**
	 * 
	 */
	isApiSet(): boolean {
		return this.api !== null;
	}

}
