import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/core/services/data.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-search-bar',
	templateUrl: './search-bar.component.html',
	styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

	/**
	 * ============================================================================================
	 * PROPERTIES
	 * ============================================================================================
	 */

	// Model for the value of the search input.
	public term: string = null;

	// A flag to allow search.
	public isActive: boolean = false;

	/**
	 * ============================================================================================
	 * CONSTRUCTOR
	 * ============================================================================================
	 */

	/**
	 * Constructor
	 * @param {DataService} data Service to fetch the data for this component's model.
	 * @param {Router} router Angular routing service.
	 */
	constructor(private data: DataService, private router: Router) { }

	/**
	 * ============================================================================================
	 * ANGULAR LIFE CYCLE
	 * ============================================================================================
	 */

	/**
	 * OnInit
	 */
	ngOnInit() {

		this.isActive = this.data.isApiSet();

		// Subscribe to api change.
		this.data.getApiChangeObservable().subscribe(apiRoute => {

			// Delay to next angular cycle. (todo)
			setTimeout(() => {

				// The search is active if an api is set.
				this.isActive = !!apiRoute
			}, 1);
		});
	}

	/**
	 * ============================================================================================
	 * PUBLIC
	 * ============================================================================================
	 */

	/**
	 * Navigate to the search page with search term as parameter.
	 */
	public doSearch() {
		if (this.term && this.isActive) {
			this.router.navigate([this.getSearchRoute()]);
		}
	}

	/**
	 * Get the search route for the current api.
	 * @returns {string}
	 */
	public getSearchRoute(): string {
		return '/api/' + this.data.getApiMeta('route') + '/search/' + this.term;
	}

}
