import { Component, OnInit, Input } from '@angular/core';
import { IItem, IItemBlob } from 'src/app/core/api/api.interface';

@Component({
	selector: 'app-item-list',
	templateUrl: './item-list.component.html',
	styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent {

	/**
	 * ============================================================================================
	 * PROPERTIESE
	 * ============================================================================================
	 */

	@Input() items: Array<IItemBlob>;

	// The array of urls of items to display in the list.
	// @Input() urls: Array<string>;

	// An optional title.
	@Input() title: string;

	// Paginate the list if provided. Useful for long list to avoid
	// sending to many requests. Default is 0 meaning no pagination.
	@Input() pagination: number = 0;

	@Input() showVerbose: boolean = true;

	public showPagination: boolean;

	// The current and maximum page.
	public page: number = 1;
	public maxPage: number = 1;

	/**
	 * ============================================================================================
	 * CONSTRUCTOR
	 * ============================================================================================
	 */
	constructor() { }

	/**
	 * ============================================================================================
	 * ANGULAR LIFE CYCLE
	 * ============================================================================================
	 */

	ngOnInit() {
		// Show pagination only if the number of items is greater than one page.
		this.showPagination = this.items ? (this.pagination < this.items.length) : false;
	}

	/**
	 * OnChanges
	 */
	ngOnChanges() {
		this.maxPage = this.pagination > 0 ? Math.ceil(this.items.length / this.pagination) : 1;
	}

	/**
	 * ============================================================================================
	 * PUBLIC
	 * ============================================================================================
	 */

	getItems() {
		if (this.pagination > 0) {
			const from: number = (this.page - 1) * this.pagination;
			const to: number = Math.min(from + this.pagination, this.items.length);
			return this.items.slice(from, to);
		}
		return this.items;
	}

	/**
	 * Display the first page of items.
	 */
	firstPage() {
		if (this.pagination > 0) {
			this.page = 1;
		}
	}

	/**
	 * Display the last page of items.
	 */
	lastPage() {
		if (this.pagination > 0) {
			this.page = this.maxPage
		}
	}

	/**
	 * Display the next page of items.
	 */
	nextPage() {
		if (this.pagination > 0) {
			this.page = Math.min(this.page + 1, this.maxPage);
		}
	}

	/**
	 * Display the previous page of items.
	 */
	previousPage() {
		if (this.pagination > 0) {
			this.page = Math.max(this.page - 1, 1);
		}
	}

	/**
	 * Returns the current pagination as a string.
	 * @returns {string}
	 */
	getVerbose(): string {
		if (!this.items) return '';
		const current: number = ((this.page - 1) * this.pagination + 1);
		const to: number = Math.min(current + this.pagination - 1, this.items.length);
		return 'showing items ' + current + ' - ' + to + ' of ' + this.items.length;
	}
}
