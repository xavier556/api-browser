import { Component, OnInit, Input } from '@angular/core';
import { DataService } from 'src/app/core/services/data.service';
import { IItem, IItemBlob } from 'src/app/core/api/api.interface';

@Component({
	selector: 'app-item-blob',
	templateUrl: './item-blob.component.html',
	styleUrls: ['./item-blob.component.scss']
})
export class ItemBlobComponent implements OnInit {

	/**
	 * ============================================================================================
	 * PROPERTIES
	 * ============================================================================================
	 */

	@Input() public itemBlob: IItemBlob = null;

	// The model for the view.
	public model: IItem = null;

	// A flag used in the view to display the item as a link.
	@Input() public linkOnly: boolean = false;

	//
	public ready: boolean = false;


	/**
	 * ============================================================================================
	 * CONSTRUCTOR
	 * ============================================================================================
	 */

	/**
	 * Constructor
	 * @param {DataService} data Service to fetch the data for this component's model.
	 */
	constructor(public data: DataService) { }

	/**
	 * ============================================================================================
	 * ANGULAR LIFE CYCLE
	 * ============================================================================================
	 */

	/**
	 * OnInit
	 */
	ngOnInit() {
		this.getModel();
	}

	/**
	 * Get the data for the model from the current api.
	 */
	getModel() {
		this.ready = false;
		this.data.getItem(this.itemBlob.resource, this.itemBlob.id).subscribe(

			// Assign result to model.
			result => {
				this.model = result;
				this.ready = true;
			},

			// Catch and display errors.
			() => {
				this.model = null;
				this.ready = true;
			});
	}

	getRoute(): string {
		return '/api/' + this.data.getApiMeta('route') + '/item/' + this.itemBlob.resource + '/' + this.itemBlob.id
	}

	// ngOnChanges(changes: SimpleChanges) {
	// 	if (changes.url && (changes.url.previousValue !== changes.url.currentValue)) {
	// 		this.loadItem();
	// 	}
	// }

}
