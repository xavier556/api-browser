import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemBlobComponent } from './item-blob.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('ItemBlobComponent', () => {
	let component: ItemBlobComponent;
	let fixture: ComponentFixture<ItemBlobComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				HttpClientTestingModule
			],
			declarations: [ItemBlobComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ItemBlobComponent);
		component = fixture.componentInstance;
		component.itemBlob = {
			id: 1,
			resource: 'resource'
		};
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
