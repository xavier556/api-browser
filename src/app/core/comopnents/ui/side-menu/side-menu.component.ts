import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/core/services/data.service';

@Component({
	selector: 'app-side-menu',
	templateUrl: './side-menu.component.html',
	styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent {

	/**
	 * ============================================================================================
	 * CONSTRUCTOR
	 * ============================================================================================
	 */

	/**
	 * Constructor
	 * @param {DataService} data Service to fetch the data for this component's model.
	 */
	constructor(private data: DataService) { }

	/**
	 * ============================================================================================
	 * PUBLIC
	 * ============================================================================================
	 */

	/**
	 * Get the list of apis. Returns an object where keys are the api technical names and
	 * values the api 'friendly' name.
	 * @returns {{[key: string]: string}}
	 */
	getListOfApi(): { [key: string]: string } {
		return this.data.listOfApi;
	}
}
