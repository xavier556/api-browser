import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CacheComponent } from './cache.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CacheComponent', () => {
	let component: CacheComponent;
	let fixture: ComponentFixture<CacheComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule
			],
			declarations: [CacheComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CacheComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
