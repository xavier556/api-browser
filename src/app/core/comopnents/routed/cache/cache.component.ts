import { Component, OnInit } from '@angular/core';
import { CacheService } from 'src/app/core/services/cache.service';
import { ApiRegistery } from 'src/app/core/api/api.registery';

@Component({
	selector: 'app-cache',
	templateUrl: './cache.component.html',
	styleUrls: ['./cache.component.scss']
})
export class CacheComponent implements OnInit {

	public listOfApi: string[];

	constructor(public cache: CacheService) {
		this.listOfApi = Object.keys(ApiRegistery);
	}

	ngOnInit() {
	}

	clearApiCache(api: string) {
		if (confirm('Clear all cacehd data for ' + api + "?")) {
			const n: number = this.cache.clearApiCache(api);
			alert(n + ' entries cleared.');
		}
	}

	loadApiCache(api: string) {
		this.cache.loadApiCache(api).subscribe(
			n => {
				alert(n + ' entires loaded to cache.');
			},
			error => {
				alert('No cache file to load.')
			});
	}

}
