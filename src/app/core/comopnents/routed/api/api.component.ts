import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/core/services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CacheService } from 'src/app/core/services/cache.service';

@Component({
	selector: 'app-api',
	templateUrl: './api.component.html',
	styleUrls: ['./api.component.scss']
})
export class ApiComponent implements OnInit {

	/**
	 * ============================================================================================
	 * PROPERTIES
	 * ============================================================================================
	 */

	/**
	 * ============================================================================================
	 * CONSTRUCTOR
	 * ============================================================================================
	 */

	/**
	 * Constructor
	 * @param {DataService} data Service to get data from this api.
	 * @param {ActivatedRoute} route Angular service to manage current route.
	 * @param {Router} router Angular router service.
	 * @param {CacheService} cache Cache service.
	 */
	constructor(
		public data: DataService,
		private route: ActivatedRoute,
		private router: Router,
		public cache: CacheService
	) { }

	/**
	 * ============================================================================================
	 * ANGULAR LIFE CYCLE
	 * ============================================================================================
	 */

	/**
	 * OnInit
	 */
	ngOnInit() {
		// Subscribe to route parameters change.
		this.route.params.subscribe(params => {
			if (params.api) {

				// Set the current api.
				this.data.setApi(params.api);
			}
			else {

				// Clear api.
				this.data.removeApi();
			}
		});
	}

	/**
	 * ============================================================================================
	 * PUBLIC
	 * ============================================================================================
	 */

	/**
	 * Check if an api is set.
	 * @returns {boolean}
	 */
	isApiSet(): boolean {
		return this.data.isApiSet();
	}

	/**
	 * Check if the current route is the base api route.
	 * @returns {boolean}
	 */
	isRootRoute(): boolean {

		return this.router.url.split('/api/')[1] === this.data.getApiMeta('route');
	}
}
