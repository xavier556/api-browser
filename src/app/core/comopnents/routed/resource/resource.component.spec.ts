import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceComponent } from './resource.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ItemListComponent } from '../../ui/item-list/item-list.component';
import { ItemBlobComponent } from '../../ui/item-blob/item-blob.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ResourceComponent', () => {
	let component: ResourceComponent;
	let fixture: ComponentFixture<ResourceComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				HttpClientTestingModule
			],
			declarations: [
				ResourceComponent,
				ItemListComponent,
				ItemBlobComponent
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ResourceComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
