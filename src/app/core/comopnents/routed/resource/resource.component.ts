import { Component, OnInit } from '@angular/core';
import { IResource } from 'src/app/core/api/api.interface';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/core/services/data.service';

@Component({
	selector: 'app-resource',
	templateUrl: './resource.component.html',
	styleUrls: ['./resource.component.scss']
})
export class ResourceComponent implements OnInit {

	/**
	 * ============================================================================================
	 * PROPERTIES
	 * ============================================================================================
	 */

	// The model for this component's view.
	public model: IResource = null;

	// A state flag that is set to true when data is available.
	public ready: boolean = false;

	// The name of the current api.
	public api: string = null;

	// The resource and page route parameters.
	public resource: string = null;
	public page: number = null;

	// Error object to display at the top of the component.
	public error: Error = null;

	/**
	 * ============================================================================================
	 * CONSTRUCTOR
	 * ============================================================================================
	 */

	/**
	 * Constructor
	 * @param {DataService} data Service to fetch the data for this component's model.
	 * @param {ActivatedRoute} route Angular service to manipulate the current route.
	 */
	constructor(public data: DataService, public route: ActivatedRoute) {
		this.api = data.getApiMeta('route');
	}

	/**
	 * ============================================================================================
	 * ANGULAR LIFE CYCLE
	 * ============================================================================================
	 */

	/**
	 * OnInit
	 */
	ngOnInit() {

		// Subscribe to route parameters changes.
		this.route.params.subscribe(params => {

			this.ready = false;
			this.error = null;
			if (params.resource && params.page) {

				// Save route parameters locally.
				this.resource = params.resource.replace(/\\/gi, '/');
				this.page = parseInt(params.page);

				// Get data for the model.
				this.getModel();
			}
			else {
				this.error = new Error('Invalid parameters.');
				this.ready = true;
			}
		});
	}

	/**
	 * ============================================================================================
	 * PRIVATE
	 * ============================================================================================
	 */

	/**
	 * Get the data for the model from the current api.
	 */
	private getModel() {
		this.ready = false;
		this.data.getResource(this.resource, this.page).subscribe(

			// Assign result to model.
			result => {
				this.model = result;
				this.ready = true;
			},

			// Catch and display error.
			error => {
				this.model = null;
				this.error = error;
				this.ready = true;
			});
	}

	/**
	 * ============================================================================================
	 * PUBLIC
	 * ============================================================================================
	 */

	/**
	 * Return the route to the next page of the resource. We can determine if
	 * there is a next page by using the 'next' property of the resource data.
	 * @return {string}
	 */
	getNextPageRoute(): string {
		if (this.model && this.model.next) {
			return `/api/${this.api}/resource/${this.model.name}/${this.model.next}`;
		}
		return '';
	}

	/**
	 * Return the route to the previous page of the resource. We can determine if
	 * there is a previous page by using the 'previous' property of the resource data.
	 * @return {string}
	 */
	getPrevPageRoute(): string {
		if (this.model && this.model.previous) {
			return `/api/${this.api}/resource/${this.model.name}/${this.model.previous}`;
		}
		return '';
	}

	/**
	 * Return the route to the last page of the resource. We can determine if
	 * there is a previous page by using the 'last' property of the resource data.
	 * @returns {string}
	 */
	getLastPageRoute(): string {
		if (this.model && this.model.last) {
			return `/api/${this.api}/resource/${this.model.name}/${this.model.last}`;
		}
		return '';
	}

	/**
	 * Return the route to the first page of the resource.
	 * @returns {string}
	 */
	getFirstPageRoute(): string {
		if (this.model && this.model.last) {
			return `/api/${this.api}/resource/${this.model.name}/1`;
		}
		return '';
	}

	/**
	 * Return the current pagination state as a string.
	 * @returns {string}
	 */
	getVerbosePagination(): string {
		const current: number = ((this.model.page - 1) * 10 + 1);
		const to: number = this.model.count ? Math.min((current + 9), this.model.count) : current + 9;
		return 'showing items ' + current + ' - ' + to + (this.model.count ? ' of ' + this.model.count : '');
	}

	/**
	 * Get the application route for the current api.
	 */
	getApiRoute(): string {
		return `/api/${this.api}`;
	}

	/**
	 * Get the application route for the resources page from the current api.
	 * @returns {string}
	 */
	getResourcesRoute(): string {
		return this.getApiRoute() + '/resources'
	}

	/**
	 * Get the application route for this resource from the current api.
	 * @returns {string}
	 */
	getResourceRoute(): string {
		return this.getApiRoute() + '/resource/' + this.resource;
	}

}
