import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchComponent } from './search.component';
import { ItemListComponent } from '../../ui/item-list/item-list.component';
import { ItemBlobComponent } from '../../ui/item-blob/item-blob.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SearchBarComponent } from '../../ui/search-bar/search-bar.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';

describe('SearchComponent', () => {
	let component: SearchComponent;
	let fixture: ComponentFixture<SearchComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				HttpClientTestingModule,
				FormsModule
			],
			declarations: [
				SearchComponent,
				SearchBarComponent,
				ItemListComponent,
				ItemBlobComponent
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SearchComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
