import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IResource, IItem } from 'src/app/core/api/api.interface';
import { DataService } from 'src/app/core/services/data.service';

@Component({
	selector: 'app-search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

	/**
	 * ============================================================================================
	 * PROPERTIES
	 * ============================================================================================
	 */

	// The search term, passed as route parameters.
	public term: string = '';

	// The model for this component's view.
	public results: Array<IResource> = null;

	// The name of the current api.
	public api: string = null;

	// A state flag that is set to true when data is available.
	public ready: boolean = false;

	// An error to display at the top of the page.
	public error: Error = null;

	/**
	 * ============================================================================================
	 * CONSTRUCTOR
	 * ============================================================================================
	 */

	/**
	 * Constructor
	 * @param {ActivatedRoute} route Angular service to manage current route.
	 * @param {DataService} data Service to get data from this api.
	 */
	constructor(private route: ActivatedRoute, private data: DataService) {
		this.api = this.data.getApiMeta('route');
	}

	/**
	 * ============================================================================================
	 * ANGULAR LIFE CYCLE
	 * ============================================================================================
	 */

	/**
	 * OnInit
	 */
	ngOnInit() {
		// Subscribe to route parameters changes.
		this.route.params.subscribe(params => {
			if (params.term) {

				// Save parameter locally.
				this.term = params.term;

				// Get the model data.
				this.getModel();
			}
		});
	}

	/**
	 * ============================================================================================
	 * PRIVATE
	 * ============================================================================================
	 */

	/**
	 * Get the data for the model from the current api.
	 */
	private getModel() {
		this.ready = false;
		this.data.search(this.term).subscribe(
			(results: Array<IResource>) => {
				this.results = results;
				this.ready = true;
			},
			error => {
				this.error = error;
				this.results = null;
				this.ready = true;
			});
	}

	/**
	 * ============================================================================================
	 * PUBLIC
	 * ============================================================================================
	 */

	/**
	 * Get the application route for the current api.
	 * @returns {string}
	 */
	getApiRoute(): string {
		return `/api/${this.api}`;
	}
}
