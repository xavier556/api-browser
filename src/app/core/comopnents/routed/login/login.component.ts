import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	/**
	 * ============================================================================================
	 * PROPERTIES
	 * ============================================================================================
	 */

	// Model for the value of the username input.
	public username: string;

	/**
	 * ============================================================================================
	 * CONSTRUCTOR
	 * ============================================================================================
	 */

	/**
	 * Constructor.
	 * @param {AuthService} auth Authenticatio service.
	 * @param {Router} router Angular routing servcie.
	 */
	constructor(public auth: AuthService, public router: Router) { }

	/**
	 * ============================================================================================
	 * ANGULAR LIFE CYCLE
	 * ============================================================================================
	 */

	/**
	 * OnInit
	 */
	ngOnInit() {
		// Redirect to home if already logged in.
		if (this.auth.isAuthenticated()) {
			this.router.navigate(['']);
		}
	}

	/**
	 * ============================================================================================
	 * PUBLIC
	 * ============================================================================================
	 */

	/**
	 * Call the auth service with provided username.
	 * The redirection is handled by the service.
	 */
	public login() {
		if (this.username) {
			this.auth.login(this.username);
		}
	}

	/**
	 * Login if the 'enter' key is pressed.
	 * @param event 
	 */
	public onKeyDown(event) {
		if (event.key === 'Enter') {
			this.login();
		}
	}
}
