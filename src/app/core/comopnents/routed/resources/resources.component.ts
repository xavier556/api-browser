import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/core/services/data.service';
import { IResource } from 'src/app/core/api/api.interface';

@Component({
	selector: 'app-resources',
	templateUrl: './resources.component.html',
	styleUrls: ['./resources.component.scss']
})
export class ResourcesComponent implements OnInit {

	/**
	 * ============================================================================================
	 * PROPERTIES
	 * ============================================================================================
	 */

	// The model for this component's view.
	public model: Array<IResource> = null;

	// A state flag that is set to true when the model is available.
	public ready: boolean = false;

	// Name of current api.
	public api: string = null;

	// Error to display at the top of the page.
	public error = null;

	/**
	 * ============================================================================================
	 * CONSTRUCTOR
	 * ============================================================================================
	 */

	/**
	 * Constructor
	 * @param {DataService} data Service to get data from this api.
	 */
	constructor(public data: DataService) {
		this.api = data.getApiMeta('route');
	}

	/**
	 * ============================================================================================
	 * ANGULAR LIFE CYCLE
	 * ============================================================================================
	 */

	/**
	 * OnInit
	 */
	ngOnInit() {
		// Get the data for the model.
		this.getModel();
	}

	/**
	 * ============================================================================================
	 * PRIVATE
	 * ============================================================================================
	 */

	/**
	 * Get the data for the model from the current api.
	 */
	private getModel() {
		this.ready = false;
		this.data.getResources().subscribe(

			// Assign result to component's model.
			resources => {
				this.model = resources;
				this.ready = true;
			},

			// Catch any error
			error => {
				this.model = null;
				this.error = error;
				this.ready = true;
			});
	}

	/**
	 * ============================================================================================
	 * PUBLIC
	 * ============================================================================================
	 */

	/**
	 * Get the application route for the current api.
	 * @returns {string}
	 */
	getApiRoute(): string {
		return `/api/${this.api}`;
	}

	/**
	 * Get the application route for the resources page from the current api.
	 * @returns {string}
	 */
	getResourcesRoute(): string {
		return this.getApiRoute() + '/resources'
	}

	/**
	 * Get the application route for a given resource from the current api.
	 * @param {string} resource The resource name.
	 * @returns {string}
	 */
	getResourceRoute(resource: string): string {
		return this.getApiRoute() + '/resource/' + resource;
	}

}
