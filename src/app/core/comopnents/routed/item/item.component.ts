import { Component, OnInit, AfterViewInit, ViewContainerRef, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { IItem } from 'src/app/core/api/api.interface';
import { DataService } from 'src/app/core/services/data.service';
import { ActivatedRoute } from '@angular/router';
import { KeyValue } from '@angular/common';
import { ApiRegistery } from 'src/app/core/api/api.registery';

@Component({
	selector: 'app-item',
	templateUrl: './item.component.html',
	styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit, AfterViewInit {

	/**
	 * ============================================================================================
	 * PROPERTIES
	 * ============================================================================================
	 */

	// Custom content container.
	@ViewChild('customContent', { read: ViewContainerRef, static: false }) customContent: ViewContainerRef;

	private isViewInit: boolean = false;

	// The model for the displayed item.
	model: IItem = null;

	// A state flag that is set to true when data is available.
	ready: boolean = false;

	// The name of the current api.
	api: string = null;

	// The resource and id route parameters.
	resource: string = null;
	id: number = null;

	// Error object to display at the top of the component.
	error: Error = null;

	/**
	 * ============================================================================================
	 * CONSTRUCTOR
	 * ============================================================================================
	 */

	/**
	 * Constructor
	 * @param {DataService} dataService Service to fetch the data for this component's model.
	 * @param {ActivatedRoute} route Angular service to manipulate the current route.
	 * @param {ComponentFactoryResolver} componentFactoryResolver 
	 */
	constructor(
		public data: DataService,
		public route: ActivatedRoute,
		private componentFactoryResolver: ComponentFactoryResolver
	) {
		this.api = this.data.getApiMeta('route');
	}

	/**
	 * ============================================================================================
	 * ANGULAR LIFE CYCLE
	 * ============================================================================================
	 */

	/**
	 * OnInit
	 */
	ngOnInit() {

		// Subscribe to route parameters changes.
		this.route.params.subscribe(params => {
			if (params.resource && params.id) {

				// Save route parameters locally.
				this.resource = params.resource.replace(/\\/gi, '/');
				this.id = parseInt(params.id);

				// Get data for the model.
				this.getModel();
			}
			else {
				this.error = new Error('Invalid parameters.');
			}
		});
	}

	/**
	 * AfterViewInit
	 */
	ngAfterViewInit() {
		setTimeout(() => {
			this.loadCustomComponent();
			this.isViewInit = true;
		}, 0);
	}

	/**
	 * ============================================================================================
	 * PRIVATE
	 * ============================================================================================
	 */

	/**
	 * Get the data for the model from the current api.
	 */
	getModel() {
		this.ready = false;
		this.data.getItem(this.resource, this.id).subscribe(

			// Assign result to model.
			result => {
				this.model = result;
				if (this.isViewInit) { this.loadCustomComponent() }
				this.ready = true;
			},

			// Catch and display errors.
			error => {
				this.model = null;
				this.error = error;
				this.ready = true;
			});
	}

	/**
	 * Load item custom component, if any.
	 */
	private loadCustomComponent() {

		// If no container returns.
		if (!this.customContent) { return; }

		// Clear the container.
		this.customContent.clear();

		// Check if there's a custom component for this resource. The component
		// must be registrered in CustomComponentRegistry. If no component exists
		// return.
		if (!ApiRegistery[this.data.getApiMeta('route')].custom) { return }
		const comp = ApiRegistery[this.data.getApiMeta('route')].custom[this.model.resource];
		if (!comp) { return; }

		// Create the custom content component set its input.
		const componentFactory = this.componentFactoryResolver.resolveComponentFactory(comp);
		const ref: any = this.customContent.createComponent(componentFactory);
		ref.instance.item = this.model;
	}

	/**
	 * ============================================================================================
	 * PUBLIC
	 * ============================================================================================
	 */

	// Preserve original property order
	originalOrder = (a: KeyValue<number, string>, b: KeyValue<number, string>): number => {
		return 0;
	}

	/**
	 * Get the application route for the current api.
	 * @returns {string}
	 */
	getApiRoute(): string {
		return `/api/${this.api}`;
	}

	/**
	 * Get the application route for the resources page from the current api.
	 * @returns {string}
	 */
	getResourcesRoute(): string {
		return this.getApiRoute() + '/resources'
	}

	/**
	 * Get the application route for this item's resource from the current api.
	 * @returns {string}
	 */
	getResourceRoute(): string {
		return this.getApiRoute() + '/resource/' + this.resource.replace(/\//gi, '\\');
	}

}
