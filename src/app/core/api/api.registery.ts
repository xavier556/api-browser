import { IafApiService } from '../../api/iaf/api/iafapi.service';
import { SwapiService } from '../../api/sw/api/swapi.service';
import { IafCharacterComponent } from '../../api/iaf/components/iaf-character.component';
import { SwFilmComponent } from '../../api/sw/components/sw-film.component';
import { PokeapiService } from '../../api/pokemon/api/pokeapi.service';
import { CocktailsApiService } from '../../api/cocktails/api/cocktails-api.service';
import { NewsapiService } from '../../api/newsapi/api/newsapi.service';

interface IRegister {

	// Reference the service class which implement the IApiInterface interface.
	service: any,

	// Api meta data like name, entry point, ...
	meta: IApiMeta

	// List of custom components.
	// The key must correspond to the resource name, the value is
	// a reference to the angular component.
	custom?: { [key: string]: any },
}

export interface IApiMeta {
	route: string,
	name: string,
	description: string,
	website: string,
	entryPoint: string
}

export const ApiRegistery: { [key: string]: IRegister } = {
	iafapi: {
		service: IafApiService,
		meta: {
			route: 'iafapi',
			name: 'An Api of Ice and Fire',
			description: 'All the data from the universe of Ice And Fire you\'ve ever wanted!',
			website: 'https://www.anapioficeandfire.com/',
			entryPoint: 'https://www.anapioficeandfire.com/api/'
		},
		custom: {
			characters: IafCharacterComponent
		}
	},
	swapi: {
		service: SwapiService,
		meta: {
			route: 'swapi',
			name: 'Star Wars Api',
			description: '',
			website: 'https://swapi.co',
			entryPoint: 'https://swapi.co/api/'
		},
		custom: {
			films: SwFilmComponent
		}
	},
	pokeapi: {
		service: PokeapiService,
		meta: {
			route: 'pokeapi',
			name: 'PokéApi',
			description: 'The RESTful Pokémon API',
			website: 'https://pokeapi.co/',
			entryPoint: 'https://pokeapi.co/api/v2/'
		}
	},
	cocktailsapi: {
		service: CocktailsApiService,
		meta: {
			route: 'cocktailsapi',
			name: 'The Cocktail DB',
			description: 'An open, crowd-sourced database of drinks and cocktails from around the world.',
			website: 'https://www.thecocktaildb.com/',
			entryPoint: 'https://www.thecocktaildb.com/api/json/v1/1/'
		}
	},
	newsapi: {
		service: NewsapiService,
		meta: {
			route: 'newsapi',
			name: 'News API',
			description: 'Search worldwide news with code',
			website: 'https://newsapi.org',
			entryPoint: 'https://newsapi.org/v2/'
		}
	}
}