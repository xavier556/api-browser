import { Observable } from 'rxjs';
import { IApiMeta } from './api.registery';

export interface IApiService {
	meta: IApiMeta,
	getItem(resource: string, id: number): Observable<IItem>
	getResource(resource: string, page: number): Observable<IResource>
	getResources(): Observable<IResource[]>
	search(term: string): Observable<IResource[]>
}

export interface IResource {
	route: string,
	name: string,
	count?: number,
	page?: number,
	next?: number,
	previous?: number,
	last?: number,
	results?: Array<IItem>
}

export interface IItem {
	name: string,
	url?: string,
	id?: number,
	resource?: string,
	image?: string,
	properties?: { [key: string]: IProperty },
	customProperties?: { [key: string]: IProperty },
	resources?: { [key: string]: IResourceLink }
}

export interface IProperty {
	title: string,
	value: any,
	format?: string
}

export interface IResourceLink {
	title: string,
	items: Array<IItemBlob>
}

export interface IItemBlob {
	id: number,
	resource: string,
	image?: string
}

export interface ICached {
	body: any,
	headers?: {
		links?: {
			prev?: string,
			next?: string,
			last?: string
		}
	}
}