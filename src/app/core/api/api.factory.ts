import { InjectionToken, Injector } from "@angular/core";

import { ApiRegistery } from "./api.registery";
import { IApiService } from './api.interface';
import { HttpClient } from '@angular/common/http';
import { CacheService } from '../services/cache.service';

export const apiNameToken = new InjectionToken<string>('ApiNameToken');
export const serviceToken = new InjectionToken<IApiService>('ApiServiceToken');

export const serviceFactory = (name: string, http: HttpClient, cache: CacheService) => {
	if (ApiRegistery[name]) {

		// Get the api meta data from the registery.
		const meta = ApiRegistery[name].meta;

		// Return a new instance of the service.
		return new ApiRegistery[name]['service'](meta, http, cache);
	}
	else {
		throw ('invalid api name: ' + name);
	}
}