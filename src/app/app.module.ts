import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ResourcesComponent } from './core/comopnents/routed/resources/resources.component';
import { ApiComponent } from './core/comopnents/routed/api/api.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './core/comopnents/routed/login/login.component';
import { SideMenuComponent } from './core/comopnents/ui/side-menu/side-menu.component';
import { ResourceComponent } from './core/comopnents/routed/resource/resource.component';
import { ItemListComponent } from './core/comopnents/ui/item-list/item-list.component';
import { ItemBlobComponent } from './core/comopnents/ui/item-blob/item-blob.component';
import { ItemComponent } from './core/comopnents/routed/item/item.component';
import { IafCharacterComponent } from './api/iaf/components/iaf-character.component';
import { SwFilmComponent } from './api/sw/components/sw-film.component';
import { SearchBarComponent } from './core/comopnents/ui/search-bar/search-bar.component';
import { SearchComponent } from './core/comopnents/routed/search/search.component';
import { CacheComponent } from './core/comopnents/routed/cache/cache.component';

@NgModule({
	declarations: [
		AppComponent,
		ResourcesComponent,
		ApiComponent,
		LoginComponent,
		SideMenuComponent,
		ResourceComponent,
		ItemListComponent,
		ItemBlobComponent,
		ItemComponent,
		IafCharacterComponent,
		SwFilmComponent,
		SearchBarComponent,
		SearchComponent,
		CacheComponent
	],
	imports: [
		FormsModule,
		BrowserModule,
		AppRoutingModule,
		HttpClientModule
	],
	providers: [],
	entryComponents: [
		IafCharacterComponent,
		SwFilmComponent
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
