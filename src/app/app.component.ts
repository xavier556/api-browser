import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthService } from './core/services/auth.service';
import { Router, RouterEvent } from '@angular/router';
import { filter } from 'rxjs/operators';
import { DataService } from './core/services/data.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

	/**
	 * ============================================================================================
	 * PROPERTIES
	 * ============================================================================================
	 */

	// App title
	public title = 'app-generic-api-browser';

	// Controls the state of the left side menu.
	public menuOpen: boolean = false;

	// Custom css based on the current api.
	public apiCss: string = '';

	/**
	 * ============================================================================================
	 * CONSTRUCTOR
	 * ============================================================================================
	 */

	/**
	 * Constructor
	 * @param {AuthService} auth Authentication service.
	 * @param {Router} router Angular routing service.
	 * @param {DataService} data Service to fetch the data for this component's model.
	 */
	constructor(public auth: AuthService, private router: Router, private data: DataService) { }

	/**
	 * ============================================================================================
	 * ANGULAR LIFE CYCLE
	 * ============================================================================================
	 */

	/**
	 * OnInit
	 */
	ngOnInit() {
		// Hook into router events to close the left side menu when navigating
		// to a new route.
		this.router.events.pipe(
			filter(e => e instanceof RouterEvent)
		).subscribe(e => {
			this.menuOpen = false;
		});

		// Subscribe to api change.
		this.data.getApiChangeObservable().subscribe(apiRoute => {

			// Avoid life cycle error... (todo)
			setTimeout(() => {

				// Update the css class for the new api.
				this.apiCss = 'api--' + apiRoute;
			}, 1);
		});
	}

}
