import { Injectable } from '@angular/core';
import { IApiService, ICached, IResource, IItem } from '../../../core/api/api.interface';
import { IApiMeta } from '../../../core/api/api.registery';
import { HttpClient } from '@angular/common/http';
import { CacheService } from 'src/app/core/services/cache.service';
import { Observable, of, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

// To replace potential '/' in resource name by '\' so it
// does not break routing.
const RegExp1 = /\//gi;

@Injectable({
	providedIn: 'root'
})
export class CocktailsApiService implements IApiService {

	/**
	 * ============================================================================================
	 * CONSTRUCTOR
	 * ============================================================================================
	 */

	/**
	 * Constructor
	 * @param http {HttpClient} Angular http service.
	 * @param cache {CacheService} Service to cache results from http requests.
	 */
	constructor(public meta: IApiMeta, private http: HttpClient, private cache: CacheService) { }

	/**
	 * ============================================================================================
	 * IMPLEMENTED
	 * ============================================================================================
	 */

	public getResources(): Observable<IResource[]> {

		// get the list of all drink categories
		const url = 'https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list';

		return this.getUrl(url).pipe(map((res: ICached) => {
			return res.body.drinks.map(el => {
				const r: IResource = {
					route: el.strCategory.replace(RegExp1, '\\'),
					name: el.strCategory
				}
				return r;
			})
		}));

	}

	public getResource(resource: string, page: number = 1): Observable<IResource> {

		const url = 'https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=' + encodeURI(resource);

		return this.getUrl(url).pipe(map((res: ICached) => {

			const r: IResource = {
				route: resource,
				name: resource,
				page: 1,
				next: 1,
				previous: 1,
				last: 1,
				count: res.body.drinks.length,
				results: res.body.drinks.map(drink => {
					const item: IItem = {
						name: drink.strDrink,
						id: drink.idDrink,
						resource: resource,
						image: drink.strDrinkThumb + '/preview'
					}
					return item;
				})
			}

			return r;

		}));
	}

	public getItem(resource: string, id: number): Observable<IItem> {
		const url = 'https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=' + id;
		return this.getUrl(url).pipe(map((res: ICached) => {
			const drink = res.body.drinks[0];
			const item: IItem = {
				name: drink.strDrink,
				id: id,
				image: drink.strDrinkThumb,
				resource: resource,
				properties: {},
			}

			for (let i = 0; i < 15; i++) {
				const key = 'strIngredient' + i;
				if (drink[key]) {
					item.properties[key] = { title: 'Ingredient ' + i, value: drink[key] + ' (' + drink['strMeasure' + i] + ')' }
				}
			}

			item.properties.glass = { title: 'Glass', value: drink.strGlass }
			item.properties.instructions = { title: 'Instruction', value: drink.strInstructions }

			return item;
		}));
	}

	public search(term: string): Observable<IResource[]> {
		return throwError(new Error('Not impplemented'));
	}

	/**
	 * ============================================================================================
	 * PRIVATE
	 * ============================================================================================
	 */

	/**
	 * Get cached dat or send an http request to the provided url and cache the result.
	 * @param url 
	 * @returns {Observable<ICached>}
	 */
	private getUrl(url: string): Observable<ICached> {
		try {
			// Try to get the data from the local cache.
			const cached = this.cache.getCachedUrl(this.meta.route, url);

			if (cached) {
				// If the url is cached return an observable of the data.
				return of(JSON.parse(cached));
			}
			else {
				// If not cached send a request to get the data.
				// The pipe is used to cache the query.
				return this.http.get(url).pipe(map(res => {

					// The object cache.
					const obj: ICached = {
						body: res,
						headers: {
							links: {}
						}
					}

					// Cache and return data.
					this.cache.cacheUrl(this.meta.route, url, JSON.stringify(obj));
					return obj;
				}));
			}
		}
		catch (e) {
			return throwError(e);
		}
	}
}
