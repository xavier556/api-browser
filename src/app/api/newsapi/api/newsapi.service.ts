import { Injectable } from '@angular/core';
import { IApiService, IResource, ICached, IItemBlob, IItem } from '../../../core/api/api.interface';
import { of, Observable, throwError } from 'rxjs';
import { IApiMeta } from '../../../core/api/api.registery';
import { HttpClient } from '@angular/common/http';
import { CacheService } from 'src/app/core/services/cache.service';
import { map } from 'rxjs/operators';

const apiKey = 'e2c41de11777480b8a306ea40437b115';

@Injectable({
	providedIn: 'root'
})
export class NewsapiService implements IApiService {

	constructor(public meta: IApiMeta, private http: HttpClient, private cache: CacheService) { }

	getItem(resource: string, id: number): Observable<IItem> {

		const url = '/item/' + resource + '/' + id;
		const cached = this.cache.getCachedUrl(this.meta.route, url);
		const obj: ICached = JSON.parse(cached);
		return of(obj.body);
	}

	getResource(resource: string, page: number): Observable<IResource> {

		const url = 'https://newsapi.org/v2/everything?qInTitle=' + resource + '&apiKey=' + apiKey + '&pageSize=10&page=' + page;

		return this.getUrl(url).pipe(map((res: ICached) => {


			const r: IResource = {
				name: resource,
				route: resource,
				count: res.body.totalResults,
				next: page + 1,
				previous: page - 1,
				page: page,
				last: Math.floor(res.body.totalResults / 10),

				results: res.body.articles.map((article, index) => {

					const id = parseInt(index + '' + Date.now());

					const item: IItem = {
						name: article.title,
						resource: resource,
						id: id,
						image: article.urlToImage,
						properties: {
							description: { title: 'Description', value: article.description },
							content: { title: 'Content', value: article.content },
							url: { title: 'Article url', value: article.url, format: 'link' },
							author: { title: 'Author', value: article.author }
						}
					}

					const cached: ICached = {
						body: item
					}

					this.cache.cacheUrl(this.meta.route, '/item/' + resource + '/' + item.id, JSON.stringify(cached));

					return item;
				}),
			}
			return r;

		}));
	}

	getResources(): Observable<IResource[]> {
		return of([
			{ route: 'penguins', name: 'Penguins' },
			{ route: 'trekking', name: 'Trekking' },
			{ route: 'santa claus', name: 'Santa Claus' },
			{ route: 'lego', name: 'Lego' },
			{ route: 'space', name: 'Space' },
			{ route: 'water', name: 'water' },
			{ route: 'tardigrade', name: 'Tardigrade' },
			{ route: 'atmospheric', name: 'Atmospheric' },
			{ route: 'ufo', name: 'Ufo' }
		]);
	}

	search(term: string): Observable<IResource[]> {

		const url = 'https://newsapi.org/v2/everything?qInTitle=' + term + '&apiKey=' + apiKey + '&pageSize=10'


		return this.getUrl(url).pipe(map((res: ICached) => {


			const r: IResource = {
				name: term,
				route: term,
				count: res.body.totalResults,

				results: res.body.articles.map((article, index) => {

					const id = parseInt(index + '' + Date.now());

					const item: IItem = {
						name: article.title,
						resource: term,
						id: id,
						image: article.urlToImage,
						properties: {
							description: { title: 'Description', value: article.description },
							content: { title: 'Content', value: article.content },
							url: { title: 'Article url', value: article.url, format: 'link' },
							author: { title: 'Author', value: article.author }
						}
					}

					const cached: ICached = {
						body: item
					}

					this.cache.cacheUrl(this.meta.route, '/item/' + term + '/' + item.id, JSON.stringify(cached));

					return item;
				}),
			}

			return [r];

		}));

	}


	/**
	 * Sends a request to the provided url and cache the result.
	 * @param url 
	 * @returns {Observable<any>}
	 */
	private getUrl(url: string): Observable<ICached> {

		try {

			// Try to get the data from the local cache.
			const cached = this.cache.getCachedUrl(this.meta.route, url);

			if (cached) {
				// If the query is cached return an observable of the data.
				return of(JSON.parse(cached));
			}
			else {
				// If not cached send a request to get the data.
				// The pipe is used to cache the query.
				return this.http.get(url)

					.pipe(map((res) => {

						const obj: ICached = { body: res };

						// Cache and return data.
						this.cache.cacheUrl(this.meta.route, url, JSON.stringify(obj));
						return obj;
					}));
			}
		}
		catch (e) {
			return throwError(e);
		}
	}
}
