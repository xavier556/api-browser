import { Injectable } from '@angular/core';
import { IApiService, IItem, IResource, ICached } from '../../../core/api/api.interface';
import { IApiMeta } from '../../../core/api/api.registery';
import { Observable, throwError, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CacheService } from 'src/app/core/services/cache.service';
import { map } from 'rxjs/operators';

const apikey = "fc6dafb6792d4e3f9dc6d47c43dcad88";

@Injectable({
	providedIn: 'root'
})
export class IpgapiService implements IApiService {


	/**
	 * Constructor
	 * @param http Angular http service.
	 */
	constructor(public meta: IApiMeta, private http: HttpClient, private cache: CacheService) { }

	getItem(resource: string, id: number): Observable<IItem> {
		throw new Error("Method not implemented.");
	}

	getResource(resource: string, page: number): Observable<IResource> {
		throw new Error("Method not implemented.");
	}

	getResources(): Observable<IResource[]> {
		throw new Error("Method not implemented.");
	}

	search(term: string): Observable<IResource[]> {
		throw new Error("Method not implemented.");
	}

	/**
	 * Sends a request to the provided url and cache the result.
	 * @param url 
	 * @returns {Observable<any>}
	 */
	private getUrl(url: string): Observable<ICached> {

		try {

			// Try to get the data from the local cache.
			const cached = this.cache.getCachedUrl(this.meta.route, url);

			if (cached) {
				// If the query is cached return an observable of the data.
				return of(JSON.parse(cached));
			}
			else {
				// If not cached send a request to get the data.
				// The pipe is used to cache the query.
				return this.http.get(url)

					.pipe(map((res) => {

						const obj: ICached = { body: res };

						// Cache and return data.
						this.cache.cacheUrl(this.meta.route, url, JSON.stringify(obj));
						return obj;
					}));
			}
		}
		catch (e) {
			return throwError(e);
		}
	}
}
