import { Injectable } from '@angular/core';
import { IApiService, IItem, IResource, ICached } from '../../../core/api/api.interface';
import { Observable, of, forkJoin, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { CacheService } from 'src/app/core/services/cache.service';
import { IApiMeta } from '../../../core/api/api.registery';

const parseLinkHeader = require('parse-link-header');

@Injectable({
	providedIn: 'root'
})
export class IafApiService implements IApiService {

	/**
	 * ============================================================================================
	 * CONSTRUCTOR
	 * ============================================================================================
	 */

	/**
	 * Constructor
	 * @param http {HttpClient} Angular http service.
	 * @param cache {CacheService} Service to cache results from http requests.
	 */
	constructor(public meta: IApiMeta, private http: HttpClient, private cache: CacheService) { }

	/**
	 * ============================================================================================
	 * IMPLEMENTED
	 * ============================================================================================
	 */

	/**
	 * Get the list of all resources.
	 * The Root resource contains information about all available resources in the API.
	 * Check the 'root' section of https://www.anapioficeandfire.com/Documentation for
	 * details on the format of the response.
	 */
	public getResources(): Observable<IResource[]> {
		return this.getUrl(this.meta.entryPoint).pipe(map((res: ICached) => {
			return Object.keys(res.body).map(key => {
				const r: IResource = {
					route: key,
					name: key
				}
				return r;
			});
		}));
	}

	/**
	 * Get a resource.
	 * Returns 10 items per page. Check the 'book' section of 
	 * https://www.anapioficeandfire.com/Documentation for details.
	 * The request url is baseUrl/:resource/?page=:page
	 * @param resource {string} The api name of the resource.
	 * @param page {number} The page number, default is 1.
	 * @returns {Observable<IResource>}
	 */
	public getResource(resource: string, page: number = 1): Observable<IResource> {

		// Build the request url.
		const pageArg: string = (page && page > 1) ? '/?page=' + page : '';
		const url = this.meta.entryPoint + resource + pageArg;

		// Get the response from the api.
		return this.getUrl(url).pipe(map((res: ICached) => {

			// Map the result to a IResource to match the service interface.
			const result: IResource = {
				route: resource,
				name: resource,
				page: Number(page),
				count: 0,
				next: Number(res.headers.links.next),
				previous: Number(res.headers.links.prev),
				last: Number(res.headers.links.last),
				results: res.body.map(r => {
					const item: IItem = {
						name: r.name,
						id: r.url.split('/').slice(-1)[0],
						resource: resource
					}
					return item;
				})
			};
			return result;
		}));
	}

	/**
	 * Get an item.
	 * The item is identified by its resource and id.
	 * The request url is baseUrl/:resource/:id
	 * @param resource {string} The api name of the item's resource.
	 * @param id {number} The item id.
	 * @returns {Observable<IItem>}
	 */
	public getItem(resource: string, id: number): Observable<IItem> {
		if (resource === 'books') return this.getBook(id);
		else if (resource === 'characters') return this.getCharacter(id);
		else if (resource === 'houses') return this.getHouses(id);
		else return throwError(new Error('Unknown resource: ' + resource));
	}


	/**
	 * Search for term in all resources.
	 * This searches for an exact match of the name of a book, character
	 * or house. The api does not provide partial match results.
	 * @param {string} term
	 * @returns {Observable<IResource[]>}
	 */
	public search(term: string): Observable<IResource[]> {

		// Get an observable of results for each resource.
		const obs = [
			this.searchResource('books', term),
			this.searchResource('characters', term),
			this.searchResource('houses', term),
		];

		// Fork join and filter empty results.
		return forkJoin(obs).pipe(map(res => res.filter(r => r.count > 0)));
	}

	/**
	 * ============================================================================================
	 * PRIVATE
	 * ============================================================================================
	 */

	/**
	 * Search for a match on a resource item name.
	 * Check filtering option on https://www.anapioficeandfire.com/Documentation for details.
	 * @param {string} resource The item resource name.
	 * @param {string} term The term to match.
	 * @returns {Observable<IResource>}
	 */
	private searchResource(resource: string, term: string): Observable<IResource> {
		const url = `${this.meta.entryPoint + resource}/?name=${term}`;
		return this.getUrl(url).pipe(map((res: ICached) => {
			const r: IResource = {
				route: resource,
				name: resource,
				results: res.body,
				count: res.body.length
			}
			return r;
		}));
	}

	/**
	 * Get a book item by id.
	 * The result is mapped to an IItem object.
	 * Check the 'books' section at https://www.anapioficeandfire.com/Documentation for details.
	 * @param {number} id The book id.
	 * @returns {Observable<IItem>}
	 */
	private getBook(id: number): Observable<IItem> {

		// Url to query the book data.
		const url = this.meta.entryPoint + 'books/' + id;

		// Get data and map to an IItem object.
		return this.getUrl(url).pipe(map((res: ICached) => {

			// Item to return.
			const item: IItem = {
				name: res.body.name,
				id: id,
				resource: 'books',
				properties: {},
				resources: {}
			}

			// Set each book property.
			item.properties.isbn = { title: 'ISBN', value: res.body.isbn };
			item.properties.authors = { title: 'Authors', value: res.body.authors.join(', ') };
			item.properties.numberOfPages = { title: 'Number of pages', value: res.body.numberOfPages };
			item.properties.publisher = { title: 'Publisher', value: res.body.publisher };
			item.properties.country = { title: 'Coutry', value: res.body.country };
			item.properties.mediaType = { title: 'Media type', value: res.body.mediaType };
			item.properties.released = { title: 'Released', value: res.body.released };

			// Set the release property format to date if present.
			if (res.body.released) { item.properties.released.format = 'date'; }

			// Add related resources if not empty.
			if (res.body.characters.length) {
				item.resources.characters = {
					title: 'Characters',
					items: res.body.characters.map(url => {
						return {
							id: url.split('/').slice(-1)[0],
							resource: 'characters'
						}
					})
				}
			}

			if (res.body.povCharacters.length) {
				item.resources.povCharacters = {
					title: 'POV Characters',
					items: res.body.povCharacters.map(url => {
						return {
							id: url.split('/').slice(-1)[0],
							resource: 'characters'
						}
					})
				}
			}

			return item;
		}));
	}

	/**
	 * Get a character item by id.
	 * The result is mapped to an IItem object.
	 * Check the 'characters' section at https://www.anapioficeandfire.com/Documentation for details.
	 * @param {number} id The character id.
	 * @returns {Observable<IItem>}
	 */
	private getCharacter(id: number): Observable<IItem> {

		// Url to query the book data.
		const url = this.meta.entryPoint + 'characters/' + id;

		// Get data and map to an IItem object.
		return this.getUrl(url).pipe(map((res: ICached) => {

			// Item to return.
			const item: IItem = {
				// Some characters don't have a name... Use first alias instead for display purpose.
				name: res.body.name || res.body.aliases[0],
				id: id,
				resource: 'characters',
				properties: {},
				resources: {}
			}

			// Set each character properties.
			item.properties.gender = { title: 'Gender', value: res.body.gender };
			item.properties.culture = { title: 'Culture', value: res.body.culture };
			item.properties.born = { title: 'Born', value: res.body.born };
			item.properties.died = { title: 'Died', value: res.body.died };
			item.properties.titles = { title: 'Titles', value: res.body.titles.join(', ') };
			item.properties.aliases = { title: 'Aliases', value: res.body.aliases.join(', ') };
			item.properties.tvSeries = { title: 'TV Series', value: res.body.tvSeries.join(', ') };
			item.properties.playedBy = { title: 'Played by', value: res.body.playedBy.join(', ') };
			item.properties.father = { title: 'Father', value: res.body.father };
			item.properties.mother = { title: 'Mother', value: res.body.mother };
			item.properties.spouse = { title: 'Spouse', value: res.body.spouse };

			if (res.body.father) {
				item.properties.father.value = {
					id: res.body.father.split('/').slice(-1)[0],
					resource: 'characters'
				}
				item.properties.father.format = 'resource'
			}

			if (res.body.mother) {
				item.properties.mother.value = {
					id: res.body.mother.split('/').slice(-1)[0],
					resource: 'characters'
				}
				item.properties.mother.format = 'resource'
			}
			if (res.body.spouse) {
				item.properties.spouse.value = {
					id: res.body.spouse.split('/').slice(-1)[0],
					resource: 'characters'
				}
				item.properties.spouse.format = 'resource'
			}

			// Add related resources if not empty.
			if (res.body.allegiances.length) {
				item.resources.allegiances = {
					title: 'Allegiance',
					items: res.body.allegiances.map(url => {
						return {
							id: url.split('/').slice(-1)[0],
							resource: 'houses'
						}
					})
				}
			}

			if (res.body.books.length) {
				item.resources.books = {
					title: 'Books',
					items: res.body.books.map(url => {
						return {
							id: url.split('/').slice(-1)[0],
							resource: 'books'
						}
					})
				}
			}

			if (res.body.povBooks.length) {
				item.resources.povBooks = {
					title: 'POV Books',
					items: res.body.povBooks.map(url => {
						return {
							id: url.split('/').slice(-1)[0],
							resource: 'books'
						}
					})
				}
			}

			return item;
		}));
	}

	/**
	 * Get a house item by id.
	 * The result is mapped to an IItem object.
	 * Check the 'houses' section at https://www.anapioficeandfire.com/Documentation for details.
	 * @param {number} id The house id.
	 * @returns {Observable<IItem>}
	 */
	private getHouses(id: number): Observable<IItem> {

		// Url to query the house data.
		const url = this.meta.entryPoint + 'houses/' + id;

		// Get data and map to an IItem object.
		return this.getUrl(url).pipe(map((res: ICached) => {

			// The item to return.
			const item: IItem = {
				name: res.body.name,
				id: id,
				resource: 'houses',
				properties: {},
				resources: {}
			}

			// Set each property.
			item.properties.region = { title: 'Region', value: res.body.region },
				item.properties.coatOfArms = { title: 'Coat Of Arms', value: res.body.coatOfArms },
				item.properties.words = { title: 'Words', value: res.body.words },
				item.properties.titles = { title: 'Titles', value: res.body.titles.join(', ') },
				item.properties.seats = { title: 'Seats', value: res.body.seats.join(', ') },
				item.properties.currentLord = { title: 'Current Lord', value: res.body.currentLord },
				item.properties.heir = { title: 'Heir', value: res.body.heir },
				item.properties.overlord = { title: 'Overlord', value: res.body.overlord },
				item.properties.founded = { title: 'Founded', value: res.body.founded },
				item.properties.founder = { title: 'Founder', value: res.body.founder },
				item.properties.diedOut = { title: 'Died out', value: res.body.diedOut },
				item.properties.ancestralWeapons = { title: 'Ancestral Weapons', value: res.body.ancestralWeapons.join(', ') }

			// Set the format to resource for properties that need to be displayed as such.
			if (res.body.currentLord) { item.properties.currentLord.format = 'resource' }
			if (res.body.heir) { item.properties.heir.format = 'resource' }
			if (res.body.overlord) { item.properties.overlord.format = 'resource' }
			if (res.body.founder) { item.properties.founder.format = 'resource' }

			// Set related resources if not empty.
			if (res.body.cadetBranches.length) {
				item.resources.cadetBranches = {
					title: 'Cadet Branches',
					items: res.body.cadetBranches.map(url => {
						return {
							id: url.split('/').slice(-1)[0],
							resource: 'houses'
						}
					})
				}
			}

			if (res.body.swornMembers.length) {
				item.resources.swornMembers = {
					title: 'Sworn Members',
					items: res.body.swornMembers.map(url => {
						return {
							id: url.split('/').slice(-1)[0],
							resource: 'characters'
						}
					})
				}
			}

			return item;
		}));
	}

	/**
	 * Get cached dat or send an http request to the provided url and cache the result.
	 * @param url 
	 * @returns {Observable<ICached>}
	 */
	private getUrl(url: string): Observable<ICached> {
		try {
			// Try to get the data from the local cache.
			const cached = this.cache.getCachedUrl(this.meta.route, url);

			if (cached) {
				// If the url is cached return an observable of the data.
				return of(JSON.parse(cached));
			}
			else {
				// If not cached send a request to get the data.
				// The pipe is used to cache the query.
				return this.http.get(url, { observe: 'response' }).pipe(map((res: HttpResponse<any>) => {

					// The object cache.
					const obj: ICached = {
						body: res.body,
						headers: {
							links: {}
						}
					}

					// The response can have links in headers.
					const headers: HttpHeaders = res.headers;
					const links = headers.get('link');
					const parsed = parseLinkHeader(links);
					if (parsed) {
						if (parsed.next) obj.headers.links.next = parsed.next.page;
						if (parsed.prev) obj.headers.links.prev = parsed.prev.page;
						if (parsed.last) obj.headers.links.last = parsed.last.page;
					}

					// Cache and return data.
					this.cache.cacheUrl(this.meta.route, url, JSON.stringify(obj));
					return obj;
				}));
			}
		}
		catch (e) {
			return throwError(e);
		}
	}
}
