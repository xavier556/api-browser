import { Component, OnInit, Input } from '@angular/core';
import { IItem } from 'src/app/core/api/api.interface';

@Component({
	selector: 'app-iaf-character',
	templateUrl: './iaf-character.component.html',
	styleUrls: ['./iaf-character.component.scss']
})
export class IafCharacterComponent implements OnInit {

	@Input() item: IItem;

	public show: boolean;

	constructor() { }

	ngOnInit() {
		// show only if one of the 3 relatives is present.
		this.show = (!!this.item.properties.spouse.value || !!this.item.properties.father.value || !!this.item.properties.mother.value);
	}


}
