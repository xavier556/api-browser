import { Component, OnInit, Input } from '@angular/core';
import { IItem } from 'src/app/core/api/api.interface';

@Component({
	selector: 'app-sw-film',
	templateUrl: './sw-film.component.html',
	styleUrls: ['./sw-film.component.scss']
})
export class SwFilmComponent implements OnInit {

	@Input() item: IItem;

	// Manage the opening crawl. The crawl array contains
	// one paragraph per item because the api returns a text
	// with \r\n\r\n characters to separate them and it is not
	// convenient to use.
	// The playCrawl boolean add/remove the 'crawl' css class.
	public crawl: Array<string> = [];
	public playCrawl: boolean = true;

	constructor() { }

	ngOnInit() {
		this.crawl = this.item.customProperties.opening_crawl.value.split('\r\n\r\n');
	}

}
