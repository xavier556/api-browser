import { Injectable } from '@angular/core';
import { IApiService, IItem, IResource, ICached } from '../../../core/api/api.interface';
import { Observable, of, forkJoin, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { CacheService } from 'src/app/core/services/cache.service';
import { IApiMeta } from '../../../core/api/api.registery';

@Injectable({
	providedIn: 'root'
})
export class SwapiService implements IApiService {

	/**
	 * Constructor
	 * @param http Angular http service.
	 */
	constructor(public meta: IApiMeta, private http: HttpClient, private cache: CacheService) { }

	/**
	 * Query to get the list of all resources.
	 * this.baseUrl
	 */
	getResources(): Observable<IResource[]> {
		return this.getUrl(this.meta.entryPoint).pipe(map((res: ICached) => {
			return Object.keys(res.body).map(key => {
				const r: IResource = {
					route: key,
					name: key
				}
				return r;
			});
		}));
	}

	/**
	 * Query to get one page of a resource.
	 * this.baseUrl/:resource/?page=:page
	 * @param resource 
	 * @param page 
	 */
	getResource(resource: string, page: number): Observable<IResource> {
		const pageArg: string = (page && page > 1) ? '/?page=' + page : '';
		const url = this.meta.entryPoint + resource + pageArg;
		return this.getUrl(url).pipe(map((res: ICached) => {
			console.log(res.body);
			const result: IResource = {
				route: resource,
				name: resource,
				page: Number(page),
				count: res.body.count,
				next: page + 1,
				previous: page - 1,
				last: Math.ceil(res.body.count / 10),
				results: res.body.results.map(r => {
					return {
						id: r.url.split('/').slice(-2)[0],
						resource: resource
					}
				})
			};
			return result;
		}));
	}

	/**
	 * Query to get on item.
	 * this.baseUrl/:resource/:id
	 * @param resource 
	 * @param id 
	 */
	getItem(resource: string, id: number): Observable<IItem> {
		if (resource === 'people') { return this.getPeople(id); }
		else if (resource === 'films') { return this.getFilm(id); }
		else if (resource === 'planets') { return this.getPlanet(id); }
		else if (resource === 'species') { return this.getSpecies(id); }
		else if (resource === 'vehicles') { return this.getVehicle(id); }
		else if (resource === 'starships') { return this.getStarship(id); }
		else { return throwError(new Error('Unknow resource: ' + resource)) }
	}

	/**
	 * 
	 * @param term
	 */
	search(term: string): Observable<IResource[]> {

		const obs = [
			this.searchResource('films', term),
			this.searchResource('species', term),
			this.searchResource('planets', term),
			this.searchResource('people', term),
			this.searchResource('vehicles', term),
			this.searchResource('starships', term),
		];

		return forkJoin(obs).pipe(map(res => res.filter(r => r.count > 0)));
	}

	/**
	 * ============================================================================================
	 * PRIVATE
	 * ============================================================================================
	 */

	/**
	 * Get a people resource by id.
	 * @param id 
	 */
	private getPeople(id: number): Observable<IItem> {
		const url = `${this.meta.entryPoint}people/${id}`;
		return this.getUrl(url).pipe(map(res => {
			const item: IItem = {
				name: res.body.name,
				url: res.body.url,
				id: id,
				resource: 'people',
				properties: {
					birth_year: { title: 'Birth Year', value: res.body.birth_year },
					eye_color: { title: 'Eye Color', value: res.body.eye_color },
					gender: { title: 'Gender', value: res.body.gender },
					hair_color: { title: 'Hair Color', value: res.body.hair_color },
					height: { title: 'Height', value: res.body.height },
					mass: { title: 'Mass', value: res.body.mass },
					skin_color: { title: 'Skin Color', value: res.body.skin_color },
					homeworld: { title: 'Homeworld', value: res.body.homeworld },
				},
				resources: {}
			}

			if (res.body.homeworld) {
				item.properties.homeworld.value = this.itemPropertyToBlob('planets', res.body.homeworld);
				item.properties.homeworld.format = 'resource';
			}

			if (res.body.species && res.body.species.length) {
				item.properties.species = {
					title: 'Species',
					value: this.itemPropertyToBlob('species', res.body.species[0]),
					format: 'resource'
				}
			}

			if (res.body.films && res.body.films.length) {
				item.resources.films = { title: 'Films', items: this.itemResourcesToBlobs('films', res.body.films) }
			}

			if (res.body.vehicles && res.body.vehicles.length) {
				item.resources.vehicles = { title: 'Vehicles', items: this.itemResourcesToBlobs('vehicles', res.body.vehicles) }
			}

			if (res.body.starships && res.body.starships.length) {
				item.resources.starships = { title: 'Starships', items: this.itemResourcesToBlobs('starships', res.body.starships) }
			}

			if (res.body.planets && res.body.planets.length) {
				item.resources.planets = { title: 'Planets', items: this.itemResourcesToBlobs('planets', res.body.planets) }
			}

			return item;
		}));
	}

	/**
	 * Get a film resource by id.
	 * @param id 
	 */
	private getFilm(id: number): Observable<IItem> {
		const url = `${this.meta.entryPoint}films/${id}`;
		return this.getUrl(url).pipe(map(res => {
			const item: IItem = {
				//api: this.route,
				name: res.body.title,
				url: res.body.url,
				id: id,
				resource: 'films',
				properties: {
					director: { title: 'Director', value: res.body.director },
					producer: { title: 'Producer', value: res.body.producer },
					release_date: { title: 'Release Date', value: res.body.release_date, format: 'date' },
					episode_id: { title: 'Episode ID', value: res.body.episode_id }
				},
				customProperties: {
					opening_crawl: { title: 'Opening Crawl', value: res.body.opening_crawl }
				},
				resources: {}
			}

			if (res.body.characters && res.body.characters.length) {
				item.resources.characters = { title: 'Characters', items: this.itemResourcesToBlobs('people', res.body.characters) };
			}
			if (res.body.planets && res.body.planets.length) {
				item.resources.planets = { title: 'Planets', items: this.itemResourcesToBlobs('planets', res.body.planets) };
			}
			if (res.body.species && res.body.species.length) {
				item.resources.species = { title: 'Species', items: this.itemResourcesToBlobs('species', res.body.species) };
			}
			if (res.body.starships && res.body.starships.length) {
				item.resources.starships = { title: 'Starships', items: this.itemResourcesToBlobs('starships', res.body.starships) };
			}
			if (res.body.vehicles && res.body.vehicles.length) {
				item.resources.vehicles = { title: 'Vehicles', items: this.itemResourcesToBlobs('vehicles', res.body.vehicles) };
			}
			return item;
		}));
	}

	/**
	 * Get a planet resource by id.
	 * @param id 
	 */
	private getPlanet(id: number): Observable<IItem> {
		const url = `${this.meta.entryPoint}planets/${id}`;
		return this.getUrl(url).pipe(map(res => {
			const item: IItem = {
				name: res.body.name,
				url: res.body.url,
				id: id,
				resource: 'planets',
				properties: {},
				resources: {}
			}

			item.properties.diameter = { title: 'Diameter [km]', value: res.body.diameter }
			item.properties.rotation_period = { title: 'Rotation Period', value: res.body.rotation_period }
			item.properties.orbital_period = { title: 'Orbital Period', value: res.body.orbital_period }
			item.properties.gravity = { title: 'Gravity', value: res.body.gravity }
			item.properties.population = { title: 'Population', value: res.body.population }
			item.properties.climate = { title: 'Climate', value: res.body.climate }
			item.properties.terrain = { title: 'Terrain', value: res.body.terrain }
			item.properties.surface_water = { title: 'Surface Water %', value: res.body.surface_water }

			if (res.body.films && res.body.films.length) {
				item.resources.films = { title: 'Films', items: this.itemResourcesToBlobs('films', res.body.films) }
			}

			if (res.body.residents && res.body.residents.length) {
				item.resources.residents = { title: 'Residents', items: this.itemResourcesToBlobs('people', res.body.residents) };
			}

			return item;
		}));
	}

	/**
	 * Get a species resource by id.
	 * @param id 
	 */
	private getSpecies(id: number): Observable<IItem> {
		const url = `${this.meta.entryPoint}species/${id}`;
		return this.getUrl(url).pipe(map(res => {
			const item: IItem = {
				name: res.body.name,
				url: res.body.url,
				id: id,
				resource: 'species',
				properties: {},
				resources: {}
			}

			item.properties.classification = { title: 'Classification', value: res.body.classification };
			item.properties.designation = { title: 'Designation', value: res.body.designation };
			item.properties.average_height = { title: 'Average Height', value: res.body.average_height };
			item.properties.average_lifespan = { title: 'Average Lifespan', value: res.body.average_lifespan };
			item.properties.eye_colors = { title: 'Eye Colors', value: res.body.eye_colors };
			item.properties.hair_colors = { title: 'Hair Colors', value: res.body.hair_colors };
			item.properties.skin_colors = { title: 'Skin Colors', value: res.body.skin_colors };
			item.properties.language = { title: 'Language', value: res.body.language };

			if (res.body.homeworld) {
				item.properties.homeworld = {
					title: 'Homeworld',
					value: this.itemPropertyToBlob('planets', res.body.homeworld),
					format: 'resource'
				}
			}

			if (res.body.people && res.body.people.length) {
				item.resources.people = { title: 'People', items: this.itemResourcesToBlobs('people', res.body.people) };
			}

			return item;
		}));
	}

	/**
	 * Get a vehicle resource by id.
	 * @param id 
	 */
	private getVehicle(id: number): Observable<IItem> {
		const url = `${this.meta.entryPoint}vehicles/${id}`;
		return this.getUrl(url).pipe(map(res => {
			const item: IItem = {
				name: res.body.name,
				url: res.body.url,
				id: id,
				resource: 'vehicles',
				properties: {},
				resources: {}
			}

			item.properties.model = { title: 'Model', value: res.body.model }
			item.properties.vehicle_class = { title: 'Vehicle Class', value: res.body.vehicle_class }
			item.properties.manufacturer = { title: 'Manufacturer', value: res.body.manufacturer }
			item.properties.length = { title: 'Length', value: res.body.length }
			item.properties.cost_in_credits = { title: 'Cost in Credits', value: res.body.cost_in_credits }
			item.properties.crew = { title: 'Crew', value: res.body.crew }
			item.properties.passengers = { title: 'Passengers', value: res.body.passengers }
			item.properties.max_atmosphering_speed = { title: 'Max Atmosphering Speed', value: res.body.max_atmosphering_speed }
			item.properties.cargo_capacity = { title: 'Cargo Capacity', value: res.body.cargo_capacity }
			item.properties.consumables = { title: 'Consumables', value: res.body.consumables }

			if (res.body.films && res.body.films.length) {
				item.resources.films = { title: 'Films', items: this.itemResourcesToBlobs('films', res.body.films) }
			}

			if (res.body.pilots && res.body.pilots.length) {
				item.resources.pilots = { title: 'Pilots', items: this.itemResourcesToBlobs('people', res.body.pilots) };
			}

			return item;
		}));
	}

	/**
	 * Get a starship resource by id.
	 * @param id 
	 */
	private getStarship(id: number): Observable<IItem> {
		const url = `${this.meta.entryPoint}starships/${id}`;
		return this.getUrl(url).pipe(map(res => {
			try {
				const item: IItem = {
					name: res.body.name,
					url: res.body.url,
					id: id,
					resource: 'starships',
					properties: {},
					resources: {}
				}

				item.properties.model = { title: 'Model', value: res.body.model }
				item.properties.starship_class = { title: 'Starship Class', value: res.body.starship_class }
				item.properties.manufacturer = { title: 'Manufacturer', value: res.body.manufacturer }
				item.properties.length = { title: 'Length', value: res.body.length }
				item.properties.cost_in_credits = { title: 'Cost in Credits', value: res.body.cost_in_credits }
				item.properties.crew = { title: 'Crew', value: res.body.crew }
				item.properties.passengers = { title: 'Passengers', value: res.body.passengers }
				item.properties.max_atmosphering_speed = { title: 'Max Atmosphering Speed', value: res.body.max_atmosphering_speed }
				item.properties.hyperdrive_rating = { title: 'Hyperdrive Rating', value: res.body.hyperdrive_rating }
				item.properties.cargo_capacity = { title: 'Cargo Capacity', value: res.body.cargo_capacity }
				item.properties.MGLT = { title: 'MGLT', value: res.body.MGLT }
				item.properties.consumables = { title: 'Consumables', value: res.body.consumables }

				if (res.body.films && res.body.films.length) {
					item.resources.films = { title: 'Films', items: this.itemResourcesToBlobs('films', res.body.films) }
				}

				if (res.body.pilots && res.body.pilots.length) {
					item.resources.pilots = { title: 'Pilots', items: this.itemResourcesToBlobs('people', res.body.pilots) };
				}

				return item;
			}
			catch (e) {
				console.log(e);
				return null;
			}
		})).pipe(catchError(error => {
			console.log(error);
			return throwError(error);
		}));
	}

	/**
	 * Query to search a resource for a term.
	 * this.baseUrl/:resource/?search=:term
	 * @param resource 
	 * @param term 
	 */
	private searchResource(resource: string, term: string): Observable<IResource> {
		const url = `${this.meta.entryPoint + resource}/?search=${term}`;
		return this.getUrl(url).pipe(map((res: ICached) => {
			const r: IResource = {
				name: resource,
				route: resource,
				count: res.body.count,
				results: res.body.results.map(el => this.itemPropertyToBlob(resource, el.url))
			}
			return r;
		}));
	}

	/**
	 * Sends a request to the provided url and cache the result.
	 * @param url 
	 * @returns {Observable<any>}
	 */
	private getUrl(url: string): Observable<ICached> {

		try {

			// Try to get the data from the local cache.
			const cached = this.cache.getCachedUrl(this.meta.route, url);

			if (cached) {
				// If the query is cached return an observable of the data.
				// console.log("cached", cachedUrl, JSON.parse(cached));
				const obj = JSON.parse(cached);
				if (obj.body) return of(obj);
				else {
					const obj2: ICached = {
						body: obj
					}
					return of(obj2);
				}
			}
			else {
				// If not cached send a request to get the data.
				// The pipe is used to cache the query.
				// console.log("http", cachedUrl);
				return this.http.get(url)

					// // 
					// .pipe(catchError(error => {
					// 	return throwError(error);
					// }))

					.pipe(map((res) => {

						const obj: ICached = { body: res };

						// Cache and return data.
						this.cache.cacheUrl(this.meta.route, url, JSON.stringify(obj));
						return obj;
					}));
			}
		}
		catch (e) {
			return throwError(e);
		}
	}

	/**
	 * Map a list of resources (urls) to an array of IBlobItem.
	 * @param resource 
	 * @param objs 
	 */
	private itemResourcesToBlobs(resource, objs) {
		return objs.map(obj => {
			return {
				id: obj.split('/').slice(-2)[0],
				resource: resource
			}
		})
	}

	/**
	 * Map one resource (url) to a IBlobItem.
	 * @param resource 
	 * @param obj 
	 */
	private itemPropertyToBlob(resource, obj) {
		return {
			id: obj.split('/').slice(-2)[0],
			resource: resource
		}
	}

}
