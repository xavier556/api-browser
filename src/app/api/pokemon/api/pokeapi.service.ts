import { Injectable } from '@angular/core';
import { IApiService, IResource, IItem, ICached, IItemBlob } from '../../../core/api/api.interface';
import { Observable, of, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { CacheService } from 'src/app/core/services/cache.service';
import { IApiMeta } from '../../../core/api/api.registery';

@Injectable({
	providedIn: 'root'
})
export class PokeapiService implements IApiService {

	/**
	 * ============================================================================================
	 * CONSTRUCTOR
	 * ============================================================================================
	 */

	/**
	 * Constructor
	 * @param meta 
	 * @param http 
	 * @param cache 
	 */
	constructor(public meta: IApiMeta, private http: HttpClient, private cache: CacheService) { }

	/**
	 * ============================================================================================
	 * IMPLEMENTED
	 * ============================================================================================
	 */

	/**
	 * 
	 */
	getResources(): Observable<IResource[]> {

		const resourceFilter = ['ability', 'berry', 'pokemon'];

		return this.getUrl(this.meta.entryPoint).pipe(map((res: ICached) => {
			return Object.keys(res.body).filter(key => resourceFilter.indexOf(key) > -1).map(key => {
				const r: IResource = {
					route: key,
					name: key
				}
				return r;
			});
		}));
	}

	/**
	 * 
	 * @param resource 
	 * @param page 
	 */
	getResource(resource: string, page: number = 1): Observable<IResource> {
		const pageArg = (page - 1) * 10;
		const url = this.meta.entryPoint + resource + '/?limit=10&offset=' + pageArg;
		return this.getUrl(url).pipe(map((res: ICached) => {
			const r: IResource = {
				route: resource,
				name: resource,
				count: res.body.count,
				page: page,
				next: res.body.next ? page + 1 : null,
				previous: res.body.previous ? page - 1 : null,
				last: Math.ceil(res.body.count / 10),
				results: res.body.results.map(r => {
					const urlParts = r.url.split('/');
					const item: IItem = {
						name: r.name,
						id: urlParts[urlParts.length - 2],
						resource: resource
					}
					return item;
				})
			}
			return r;
		}));
	}

	/**
	* Get an item.
	* The item is identified by its resource and id.
	* @param resource {string} The api name of the item's resource.
	* @param id {number} The item id.
	* @returns {Observable<IItem>}
	*/
	getItem(resource: string, id: number): Observable<IItem> {


		if (resource === 'berry') return this.getItemBerry(id);
		else if (resource === 'pokemon') return this.getItemPokemon(id);
		else if (resource === 'ability') return this.getItemAbility(id);
		else if (resource === 'pokemon-species') return this.getItemSpecies(id);

		else {
			const url = this.meta.entryPoint + resource + '/' + id;
			return this.getUrl(url).pipe(map((res: ICached) => {
				const item: IItem = {
					name: res.body.name,
					resource: resource,
					id: id
				}
				return item;
			}));
		}
	}

	/**
	 * 
	 * @param term 
	 */
	search(term: string): Observable<IResource[]> {
		return throwError(new Error('Not implemented.'));
	}

	/**
	 * ============================================================================================
	 * PRIVATE
	 * ============================================================================================
	 */

	/**
	 * Return an item from the pokemon resource.
	 * @param id 
	 */
	private getItemPokemon(id: number): Observable<IItem> {
		const url = this.meta.entryPoint + 'pokemon/' + id;
		return this.getUrl(url).pipe(map((res: ICached) => {
			const item: IItem = {
				name: res.body.name,
				url: res.body.url,
				resource: 'pokemon',
				id: id,
				properties: {
					base_experience: { title: 'Weight', value: res.body.base_experience },
					weight: { title: 'Weight', value: res.body.weight }
				},
				resources: {}
			}

			// species
			item.properties.species = { title: 'Species', value: this.namedAPIResourceToBlobItem('pokemon-species', res.body.species), format: 'resource' }
			console.log(item.properties.species);
			// abilities
			item.resources.abilities = {
				title: 'Abilities',
				items: res.body.abilities.map(el => this.namedAPIResourceToBlobItem('ability', el.ability))
			}

			return item;
		}));
	}

	/**
	 * Return an item from the pokemon resource.
	 * @param id 
	 */
	private getItemAbility(id: number): Observable<IItem> {
		const url = this.meta.entryPoint + 'ability/' + id;
		return this.getUrl(url).pipe(map((res: ICached) => {
			const item: IItem = {
				name: res.body.name,
				url: res.body.url,
				resource: 'ability',
				id: id,
				properties: {
					effect: { title: 'Effect', value: res.body.effect_entries[0].effect },
					short_effect: { title: 'Short Effect', value: res.body.effect_entries[0].short_effect }
				},
				resources: {
				}
			}

			// pokemons
			item.resources.pokemon = {
				title: 'Pokemons',
				items: res.body.pokemon.map(el => this.namedAPIResourceToBlobItem('pokemon', el.pokemon))
			}

			return item;
		}));
	}

	/**
	 * Return an item from the berry resource.
	 * @param id 
	 */
	private getItemBerry(id: number): Observable<IItem> {
		const url = this.meta.entryPoint + 'berry/' + id;
		return this.getUrl(url).pipe(map((res: ICached) => {
			const item: IItem = {
				name: res.body.name,
				url: res.body.url,
				resource: 'berry',
				id: id,
				properties: {
					growth_time: { title: 'Growth Time', value: res.body.growth_time },
					max_harvest: { title: 'Max Harvest', value: res.body.max_harvest },
					natural_gift_power: { title: 'Natural Gift Power', value: res.body.natural_gift_power },
					size: { title: 'Size [mm]', value: res.body.size },
					smoothness: { title: 'Smoothness', value: res.body.smoothness }
				}
			}
			return item;
		}));
	}

	/**
	 * Return an item from the pokemon resource.
	 * @param id 
	 */
	private getItemSpecies(id: number): Observable<IItem> {
		const url = this.meta.entryPoint + 'pokemon-species/' + id;
		return this.getUrl(url).pipe(map((res: ICached) => {
			const item: IItem = {
				name: res.body.name,
				url: res.body.url,
				resource: 'pokemon-species',
				id: id,
				properties: {},
				resources: {}
			}

			return item;
		}));
	}

	/**
	 * Sends a request to the provided url and cache the result.
	 * @param url 
	 * @returns {Observable<any>}
	 */
	private getUrl(url: string): Observable<ICached> {

		// Try to get the data from the local cache.
		const cached = this.cache.getCachedUrl(this.meta.route, url);

		if (cached) {
			// If the query is cached return an observable of the data.
			// console.log("cached", cachedUrl, JSON.parse(cached));
			return of(JSON.parse(cached));
		}
		else {
			// If not cached send a request to get the data.
			// The pipe is used to cache the query.
			// console.log("http", cachedUrl);
			return this.http.get(url).pipe(map((res) => {

				const obj: ICached = { body: res };

				// Cache and return data.
				this.cache.cacheUrl(this.meta.route, url, JSON.stringify(obj))
				return obj;
			}));
		}
	}


	private namedAPIResourceToBlobItem(resource: string, obj: any): IItemBlob {
		return {
			id: obj.url.split('/').slice(-2)[0],
			resource: resource
		}
	}

}
